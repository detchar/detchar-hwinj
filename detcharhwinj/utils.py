#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# The MIT License (MIT)
# Copyright (c) 2018 LIGO Scientific Collaboration
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
# DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
# OR OTHER DEALINGS IN THE SOFTWARE.

"""A simple library for generic utilities useful for detchar hardware injection tasks.
"""
__author__ = "Reed Essick <reed.essick@ligo.org>"

#-------------------------------------------------

from detcharwaveforms import waveforms

#-------------------------------------------------

DEFAULT_PAD = DEFAULT_BUFFER = 10 # sec
DEFAULT_SRATE = 16384 # Hz

KNOWN_INSTRUMENTS = ['H1', 'L1', 'V1', 'G1']
DEFAULT_INSTRUMENT = KNOWN_INSTRUMENTS[0]

DEFAULT_SOURCE_CHANNEL = DEFAULT_DESTINATION_CHANNEL = None

#-------------------------------------------------

def _check_name(waves, name):
    if not name in waves:
        raise KeyError('waveform (%s) not recognized!'%name)

def parse_sequence(waves, sequence):
    name = sequence.pop(0)
    _check_name(waves, name)
    inj = InjectionSequence(waves[name])
    while len(sequence):
        dt = sequence.pop(0)
        name = sequence.pop(0)
        _check_name(waves, name)
        inj.append(waves[name], dt)
    return inj

def parse_regular_sequence(waves, dt, num, name):
    _check_name(waves, name)
    wave = waves[name]
    assert num>0, 'must specify at least 1 injection per sequence'
    sequence = InjectionSequence(wave)
    for _ in range(num-1):
        sequence.append(wave, dt)
    return sequence

def parse_poisson_sequence(waves, rate, num, name):
    _check_name(waves, name)
    wave = waves[name]
    assert num>0, 'must specify at least 1 injection per sequence'
    sequence = InjectionSequence(wave)
    for dt in -np.log(1-np.random.rand(num-1))/rate:
        sequence.append(wave, dt)
    return sequence

#-------------------------------------------------

class Injection(object):
    """an object that stores a pointer to a waveform (expected to be callable), a central time, and a GraceID
    """

    def __init__(self, instrument, waveform, t0, graceid=None, source_channel=DEFAULT_SOURCE_CHANNEL, destination_channel=DEFAULT_DESTINATION_CHANNEL):
        assert instrument in KNOWN_INSTRUMENTS, 'instrument must be one of: '+', '.join(KNOWN_INSTRUMENTS)
        self._instrument = instrument
        self._source_channel = source_channel
        self._destination_channel = destination_channel
        assert isinstance(waveform, waveforms.Waveform), 'waveform must be an instance of '+type(waveforms.Waveform)
        self._waveform = waveform ### define these as properties because I want them to be "protected"
        self._t0 = t0
        self._graceid = None

    @property
    def instrument(self):
        return self._instrument

    @property
    def source_channel(self):
        return self._source_channel

    @property
    def destination_channel(self):
        return self._destination_channel

    @property
    def waveform(self):
        return self._waveform

    @property
    def t0(self):
        return self._t0

    @property
    def graceid(self):
        return self._graceid

    @graceid.setter
    def graceid(self, new):
        if self._graceid is not None:
            raise RuntimeError('graceid is already set to %s. Cannot set graceid for an injection more than once!'%self._graceid)
        self._graceid = new

    def __call__(self, times):
        return self.waveform.time_domain(times, self.t0)

class InjectionSequence(object):
    """an object that stores relevant information for a sequence of injections
    """
    def __init__(self, waveform):
        self._placement = []
        self._cumulative_offset = 0.
        self.append(waveform, 0.)

    def append(self, waveform, dt):
        self._cumulative_offset += dt ### bump this by the appropriate ammount
        self._placement.append((waveform, self._cumulative_offset)) ### record the cumulative offset along with a pointer to waveform

    def schedule(self, instrument, t0, source_channel=DEFAULT_SOURCE_CHANNEL, destination_channel=DEFAULT_DESTINATION_CHANNEL):
        return [Injection(instrument, waveform, t0+dt, source_channel=source_channel, destination_channel=destination_channel) for waveform, dt in self._placement]

#-------------------------------------------------

def parse_waveform(**kwargs):
    """performs type casting, etc. returns waveform_type, items
    """
    if 'type' not in kwargs:
        raise KeyError('must specify "type" as an option for each waveform')
    waveform_type = kwargs.pop('type')

    items = {}
    for key, value in kwargs.items():
        try:
            value = int(value)
        except Exception as e:
            try:
                value = float(value)
            except Exception as e:
                pass
        items[key] = value

    return waveform_type, items

def schedule(instrument, sequences, start, pad=DEFAULT_PAD, buffer=DEFAULT_BUFFER, source_channel=DEFAULT_SOURCE_CHANNEL, destination_channel=DEFAULT_DESTINATION_CHANNEL):
    """map a list of sequences into a list of Injections
    """
    t0 = start + pad

    injections = []
    for sequence in sequences:
        injections += sequence.schedule(instrument, t0, source_channel=source_channel, destination_channel=destination_channel)
        t0 = injections[-1].t0 + buffer

    return start, t0+(pad-buffer), injections
