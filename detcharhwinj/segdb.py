#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# The MIT License (MIT)
# Copyright (c) 2018 LIGO Scientific Collaboration
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
# DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
# OR OTHER DEALINGS IN THE SOFTWARE.

"""A simple library for interacting with SegDb
"""
__author__ = "Reed Essick <reed.essick@ligo.org>"

#-------------------------------------------------

### lsc-specific packages
try:
    from dqsegdb.apicalls import dqsegdbQueryTimes
except ImportError:
    dqsegdbQueryTimes = None
try:
    from ligo import segments as ligo_segments
except ImportError:
    ligo_segments = None

#-------------------------------------------------

DEFAULT_SEGDB_URL = 'https://segments.ligo.org'
DEFAULT_WINDOW = 10 # seconds, used to pad gps times read from GraceDb to set up queries

#-------------------------------------------------

def query(gps, flag, window=DEFAULT_WINDOW, segdb_url=DEFAULT_SEGDB_URL):
    """query SegDb to determine whether flag was known and active at gps
    """
    if dqsegdbQueryTimes is None:
        raise ImportError('could not import dqsegdb.apicalls.dqsegdbQueryTimes')
    if ligo_segments is None:
        raise ImportError('could not import ligo.segments')

    # set up query
    start = gps - window
    end = gps + window

    protocol, url = segdb_url.split("://")
    ifo, name, ver = flag.split(":")

    # perform query
    resp = dqsegdbQueryTimes(
        protocol=protocol,
        server=url,
        ifo=ifo,
        name=name,
        version=int(ver),
        include_list_string='active',
        startTime=start,
        endTime=end,
    )[0]

    # extract segments
    segs = ligo_segments.segmentlist(map(ligo_segments.segment, resp["active"])).coalesce()

    # determine if flag was active at gps
    for s, e in segs:
        if (s<=gps) and (gps<e): # gps is within this segment, so return true
            return True

    return False ### if we're still here, gps was not contained in any segment
