#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# The MIT License (MIT)
# Copyright (c) 2018 LIGO Scientific Collaboration
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
# DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
# OR OTHER DEALINGS IN THE SOFTWARE.

"""A simple library for interacting with GraceDb.
"""
__author__ = "Reed Essick <reed.essick@ligo.org>"

#-------------------------------------------------

import os
import json
import tempfile

### lsc-specific packages
try:
    from ligo.gracedb.rest import GraceDb, DEFAULT_SERVICE_URL as DEFAULT_GRACEDB_URL
except ImportError:
    GraceDb = None
    DEFAULT_GRACEDB_URL = 'None'

try:
    from glue.ligolw import ligolw
    from glue.ligolw import utils as ligolw_utils
    from glue.ligolw import lsctables
    from glue.ligolw import ilwd
except ImportError:
    ligolw = None

### non-standard libraries
from . import utils

#-------------------------------------------------

INJECTION_LOG_MESSAGE = 'detchar hardware injection parameters: '
#INJECTION_XML_TEMPLATE = 'detchar-hwinj-%d.xml.gz'
INJECTION_XML_TEMPLATE = 'detchar-hwinj-%d.xml'
INJECTION_JSON_NAME = 'detchar-hwinj.json'
INJECTION_TAG_NAMES = ['detchar-hwinj']

PIPELINE = 'HardwareInjection'
DEFAULT_GROUP = 'Test'

CREATION_LABELS = ['INJ', 'HWINJREQ']
SUCCESS_LABELS = {'remove': ['HWINJREQ'], 'apply':['HWINJOK']}
FAILURE_LABELS = {'remove': ['HWINJREQ'], 'apply':['HWINJNO']}

#-------------------------------------------------

def init(url=DEFAULT_GRACEDB_URL):
    """a nearly trivial wrappper around GraceDb client-side instantiation, which could be extended easily to support things like FakeDb
    """
    if GraceDb is None:
        raise ImportError('could not import ligo.gracedb.rest.GraceDb')
    return GraceDb(url)

def create(client, injection, verbose=False, group=DEFAULT_GROUP):
    """create a new entry in GraceDb (client) associated with hwinj_params and annoate as needed. Returns the GraceID
    """
    if ligolw is None:
        raise ImportError('could not import ligolw, which is needed to generate simulated coinc tables')
    assert isinstance(injection, utils.Injection), 'injection must be an instance of '+type(utils.Injection)

    ### create entry in GraceDb
    if verbose:
        print('    creating GraceDb entry for %d'%hash(injection))

    kwargs = {'instrument':injection.instrument}
    if injection.source_channel is not None:
        kwargs['source_channel'] = injection.source_channel
    if injection.destination_channel is not None:
        kwargs['destination_channel'] = injection.destination_channel
    response = client.createEvent(
        group,
        PIPELINE,
        INJECTION_XML_TEMPLATE%hash(injection),
        filecontents=siminspiral_xml_doc(injection.t0, injection.instrument),
        labels=CREATION_LABELS,
        **kwargs
    )
    injection.graceid = response.json()['graceid']
    if verbose:
        print('    injection assigned: '+injection.graceid)

    ### upload json parameters
    params = injection.waveform.params
    params['waveform'] = injection.waveform.name
    params['t0'] = injection.t0
    client.writeLog(injection.graceid, INJECTION_LOG_MESSAGE, filename=INJECTION_JSON_NAME, filecontents=json.dumps(params), tag_name=INJECTION_TAG_NAMES)
    if verbose:
        print('    annotated %s with injection parameters: %s'%(injection.graceid, INJECTION_JSON_NAME))

def siminspiral_xml_doc(t0, instrument):
    """generate a dummy xml coinc table so GraceDb doesn't cough when we create events
    """
    xmldoc = ligolw.Document()
    xml_element = ligolw.LIGO_LW()
    xmldoc.appendChild(xml_element)

    ### generate CoincInspiralTable
    ifo = instrument[0].lower()
    end_time = '%s_end_time'%ifo ### do this so we use the correct IFO
    end_time_ns = '%s_end_time_ns'%ifo
    siminspiral = lsctables.New(lsctables.SimInspiralTable, columns=['simulation_id', 'geocent_end_time', 'geocent_end_time_ns', end_time, end_time_ns])
    xml_element.appendChild(siminspiral)

    # add a single row to that table
    row = lsctables.SimInspiral()
    row.simulation_id = ilwd.sim_inspiral_simulation_id_class(0)
    row.geocent_end_time = int(t0)
    row.geocent_end_time_ns = 1e9*(t0 - row.geocent_end_time)
    row.__setattr__(end_time, row.geocent_end_time)
    row.__setattr__(end_time_ns, row.geocent_end_time_ns)

    siminspiral.append(row)

    temp = tempfile.TemporaryFile()
    ligolw_utils.write_fileobj(xmldoc, temp, gz=INJECTION_XML_TEMPLATE.endswith('.gz'))
    temp.seek(0,0)
    return temp.read()

def success(client, graceid, verbose=False):
    """update the GraceId associated with an injection to denote that it was successfully injected
    """
    _update(client, graceid, SUCCESS_LABELS, verbose=verbose)


def failure(client, graceid, verbose=False):
    """update the GraceId associated with an injection to denot that it was not successfully injected
    """
    _update(client, graceid, FAILURE_LABELS, verbose=verbose)

def _update(client, graceid, labels, verbose=False):
    """a simple helper function to remove and apply labels
    """
    for label in labels['remove']:
        if verbose:
            print('removing label=%s from %s'%(label, graceid))
        client.removeLabel(graceid, label)
    for label in labels['apply']:
        if verbose:
            print('applying label=%s to %s'%(label, graceid))
        client.writeLabel(graceid, label)

def gps(client, graceid, verbose=False):
    """query client for graceid's gpstime
    """
    if verbose:
        print('querying gpstime corresponding to %s'%graceid)
    if (graceid[0]=='S') or (graceid[:2]=='TS'):
        event = client.superevent(graceid).json()
        return event['t_0']
    else:
        event = client.event(graceid).json()
        return event['gpstime']
