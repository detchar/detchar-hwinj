# detchar-hwinj

Standard code to generate injection waveforms, scheduling actual injections, and managing parameters et al for events in GraceDb.
The goal is to provide a one-stop-shop for quickly generating, scheduling, and annotating HW injections from front-end machines.
This will ease the process of automated DetChar HW injections and the associated follow-up.

We note that the DetChar group has stated specifications for these injections [here](https://dcc.ligo.org/LIGO-T1800252).

### executables

**detchar-hwinj-schedule**

This script allows users to generate a injections by specifying several sequences of waveforms.
Waveform parameters are extracted from an INI file (`config_path`) and distributed as requested via the `--sequence`, `--regular-sequence`, and `--poisson-sequence` options; `--pad` and `--buffer` control the separation between adjacent sequences.
In this way, users can quickly define a series of injection with only a few command line arguments, e.g.:

```
$ detchar-hwinj-schedule 
    -V \
    --regular-sequence 1 10 "sine gaussian q: 100Hz+10" \
    --regular-sequence 1 10 "sine gaussian q: 500Hz+10" \
    --regular-sequence 1 10 "sine gaussian q: 1000Hz+10" \
    ${config_path} ${gps_start_time}
```

In addition to generating timeseries output and generating a log of which injections are within the timeseries, `detchar-hwinj-schedule` will also create GraceDb events (pipeline="HardwareInjection") for each injection scheduled.
These events will automatically be annotated with the injection parameters, sufficient to re-create the injected h(t), as well as labeled denoting an injection was requested (`HWINJREQ`).

The full help-string is reported below.
```
usage: detchar-hwinj-schedule [-h] [-v] [-V] [--pad PAD] [--buffer BUFFER]
                              [--sequence SEQUENCE [SEQUENCE ...]]
                              [--regular-sequence REGULAR_SEQUENCE REGULAR_SEQUENCE REGULAR_SEQUENCE]
                              [--poisson-sequence POISSON_SEQUENCE POISSON_SEQUENCE POISSON_SEQUENCE]
                              [-s SRATE] [--output-type OUTPUT_TYPE]
                              [-o OUTPUT_DIR] [-t TAG]
                              [--gracedb-url GRACEDB_URL] [--ifo IFO]
                              [--group GROUP] [--do-not-annotate-gracedb]
                              config_path start

A script to automatically generate and schedule hardware injections and create
GraceDb entries as needed, annotating each entry with standard parameter
files. NOTE: sequences specified via --sequence are added before those
specified via --regular-sequence. Within each group, sequences are added in
the order in which they appear in the command line

optional arguments:
  -h, --help            show this help message and exit

required arguments:
  config_path           path to an INI config file specifying the waveforms
                        and their parameters used in the sequences specified
                        via --sequence and --regular-sequence
  start                 the gps time at which the first sequence should start

verbosity arguments:
  -v, --verbose
  -V, --Verbose

sequence arguments:
  --pad PAD             the number of seconds padded before first sequence and
                        after the final sequence. DEFAULT=10.000
  --buffer BUFFER       the number of seconds between sequences.
                        DEFAULT=10.000
  --sequence SEQUENCE [SEQUENCE ...]
                        --sequence name [dt name dt name dt name...]
  --regular-sequence REGULAR_SEQUENCE REGULAR_SEQUENCE REGULAR_SEQUENCE
                        --regular-sequence dt num name
  --poisson-sequence POISSON_SEQUENCE POISSON_SEQUENCE POISSON_SEQUENCE
                        --poisson-sequence rate num name

output arguments:
  -s SRATE, --srate SRATE
                        the sample rate for the resulting timeseries (in Hz).
                        DEFAULT=16384.000
  --output-type OUTPUT_TYPE
                        the type of output file. Can be one of: txt, npy,
                        hdf5, gwf. DEFAULT=txt
  -o OUTPUT_DIR, --output-dir OUTPUT_DIR
  -t TAG, --tag TAG
  --gracedb-url GRACEDB_URL
                        DEFAULT=https://gracedb.ligo.org/api/
  --ifo IFO             the instrument used when creating events in GraceDb.
                        DEFAULT=None
  --group GROUP         The "group" used when creating events in GraceDb.
                        DEFAULT=Burst
  --do-not-annotate-gracedb
                        do not create events in GraceDb corresponding to the
                        scheduled injections. The DEFAULT behavior is to
                        always create events.
```

**detchar-hwinj-snr**

This script allows users to estimate the SNR of their injection sequence before createing it with `detchar-hwinj-schedule`.
The command-line syntax is similar to that of `detchar-hwinj-schedule` so that one can copy-and-paste as much of it as possible.

For example, the analogous command line for the previous example would be
```
$ detchar-hwinj-snr
    -V \
    --regular-sequence 1 10 "sine gaussian q: 100Hz+10" \
    --regular-sequence 1 10 "sine gaussian q: 500Hz+10" \
    --regular-sequence 1 10 "sine gaussian q: 1000Hz+10" \
    ${config_path} ${psd_path}
```

The full help-string is reported below.
```
usage: detchar-hwinj-snr [-h] [-v] [-V] [--pad PAD] [--buffer BUFFER]
                         [--sequence SEQUENCE [SEQUENCE ...]]
                         [--regular-sequence REGULAR_SEQUENCE REGULAR_SEQUENCE REGULAR_SEQUENCE]
                         [--poisson-sequence POISSON_SEQUENCE POISSON_SEQUENCE POISSON_SEQUENCE]
                         config_path psd_path

A script to compute the optimal SNR of each element of an injection set given
a PSD. Should be useful to check injections to be generated with detchar-
hwinj-schedule. NOTE: sequences specified via --sequence are added before
those specified via --regular-sequence. Within each group, sequences are added
in the order in which they appear in the command line

optional arguments:
  -h, --help            show this help message and exit

required arguments:
  config_path           path to an INI config file specifying the waveforms
                        and their parameters used in the sequences specified
                        via --sequence and --regular-sequence
  psd_path              the path to a (one-sided) PSD file with rows stored as
                        "frequency PSD". Used to compute SNRs of each
                        injection

verbosity arguments:
  -v, --verbose
  -V, --Verbose

sequence arguments:
  --pad PAD             the number of seconds padded before first sequence and
                        after the final sequence. DEFAULT=10.000
  --buffer BUFFER       the number of seconds between sequences.
                        DEFAULT=10.000
  --sequence SEQUENCE [SEQUENCE ...]
                        --sequence name [dt name dt name dt name...]
  --regular-sequence REGULAR_SEQUENCE REGULAR_SEQUENCE REGULAR_SEQUENCE
                        --regular-sequence dt num name
  --poisson-sequence POISSON_SEQUENCE POISSON_SEQUENCE POISSON_SEQUENCE
                        --poisson-sequence rate num name

output arguments:
  -o OUTPUT_DIR, --output-dir OUTPUT_DIR
  -t TAG, --tag TAG
  -p, --plot
  --xlim XLIM XLIM      the xlimits for the plot
  --ylim YLIM YLIM      the ylimits for the plot
```

**detchar-hwinj-query**

This script is meant so simplify GraceDb queries and filtering based on waveform parameters (and type).
While `detchar-hwinj-schedule` will be the workhorse for generating new HW injections, `detchar-hwinj-query` will be the main tool to identify injections and provide input to automated follow-up like safety studies.
We note that most of the functionality in this script is supported by the SQL interfact provied under `ligo.gracedb.rest.GraceDb.events`, and the only new behavior involves automatically pulling down JSON parameter files and filtering based on those.

To filter based on a waveform and parameters, users should pass the `--waveform` and `--param-range` flags, repeating the `--param-range` for each condition.
Parameters that do not have a range specified will be ignored when filtering events.
This will return **only** those events that
  * have detchar-hwinj JSON parameter files
  * corresponed to the requested waveform
  * have all their values within the requested ranges

For example, the result from some test events is shown below (Note: it is unlikely you will be able to reproduce this exact result, but it demonstrate's the expected behavior).
```
$ detchar-hwinj-query 1231097500 1231097600 \
    -V \
    --waveform SineGaussianTau \
    --param f 0 1000 \
  graceid        gpstime        waveform         a       tau         f       phi
    T0628 1231097527.000 SineGaussianTau 1.000e-22 1.000e-01 1.000e+02 0.000e+00
    T0629 1231097553.607 SineGaussianTau 1.000e-22 1.000e-01 1.000e+02 0.000e+00
```

The full help-string is reported below.
```
usage: detchar-hwinj-query [-h] [-v] [-V] [--delimiter DELIMITER]
                           [--gracedb-url GRACEDB_URL] [--group GROUP]
                           [--instrument INSTRUMENT] [--waveform WAVEFORM]
                           [--param-range PARAM_RANGE PARAM_RANGE PARAM_RANGE]
                           start end

A script to automatically discover hardware injections and return their
parameters. Supports some basic query language, although not full SQL.

optional arguments:
  -h, --help            show this help message and exit

required arguments:
  start                 the gps start time for our query
  end                   the gps end time for our query

verbosity arguments:
  -v, --verbose
  -V, --Verbose
  --delimiter DELIMITER
                        delimiter for table printed at the end of the query.
                        DEFAULT=" "

query arguments:
  --gracedb-url GRACEDB_URL
                        DEFAULT=https://gracedb.ligo.org/api/
  --group GROUP         return only injections with this group. If not
                        specified, will return all injections regardless of
                        group.
  --instrument INSTRUMENT
                        return only injections within this instrument. If not
                        specified, will return all injections regardless of
                        instrument.
  --waveform WAVEFORM   return only hwinj that are associated with this
                        waveform. This also requires there to be a detchar-
                        hwinj.json associated with the event.
  --param-range PARAM_RANGE PARAM_RANGE PARAM_RANGE
                        return only those events that have params with this
                        range, eg. "--param-range f 10 100". This is ignored
                        unless --waveform is also specified. This can be
                        repeated to filter based on multiple parameters.
```

**detchar-hwinj-update**

This script performs basic follow-up queries to determine whether an injection was successful.
**In particular, **`detchar-hwinj-update`** assumes all injections already have a GraceID and that injections are only successful (**`HWINJOK`**) if they live within an existing flag in SegDb.
If injections are not contained within the existing flag, they are labeled as failures (**`HWINJNO`**).**
We do *not* check the actual excitation channels for consistency between injected and expected waveforms and instead rely on Guardian processes to simply determine when something was injected, summarizing the result in a known flag.

Users must specify the set of GraceIDs they would like to update along with the flag used to define successful injections.

The full help-string is reported below.
```
usage: detchar-hwinj-update [-h] [-v] [-V] [--segdb-url SEGDB_URL]
                            [--gracedb-url GRACEDB_URL]
                            [--do-not-annotate-gracedb]
                            [graceids [graceids ...]] flag

A script to automatically update GraceDb events to record whether HW
injections were successful. This is done by checking for transient injection
segments containing the relevant events

optional arguments:
  -h, --help            show this help message and exit

required arguments:
  graceids              the graceids for which we want to check the injection
                        status

verbosity arguments:
  -v, --verbose
  -V, --Verbose

segment arguments:
  flag                  the SegDB flag used to define stretches of transient
                        injections and therefore determine success or failure
                        of an injection.
  --segdb-url SEGDB_URL
                        DEFUALT=https://segments.ligo.org

gracedb arguments:
  --gracedb-url GRACEDB_URL
                        DEFAULT=https://gracedb.ligo.org/api/
  --do-not-annotate-gracedb
                        do not remove or apply labels to events in GraceDb.
                        The DEFAULT behavior is to always update labels.
```

**detchar-hwinj-add2channel**

This script will perform software injections based on the output of `detchar-hwinj-schedule` and a target frame.
Such injections can be useful when determining whether an Event Trigger Generator (ETG) can detect a specific sequence of injections before investing in an actual hardware injection campaign.
The resulting time-series is written into a new frame, which should be suitable input for most, if not all, ETGs.

The full help-string is reported below.
```
$ detchar-hwinj-add2channel -h
usage: detchar-hwinj-add2channel [-h] [-v VERBOSE] [-o OUTPUT_DIR] [-t TAG]
                                 injection target_frame target_channel

A script to add an injection sequence produced by detchar-hwinj-schedule into
an existing frame, writing out the result as a new frame

optional arguments:
  -h, --help            show this help message and exit

required arguments:
  injection             path to output series from detchar-hwinj-schedule
  target_frame          path to the target frame from which we will read the
                        target channel
  target_channel        the channel into which we will inject the series from
                        "injection"

verbosity arguments:
  -v VERBOSE, --verbose VERBOSE

output arguments:
  -o OUTPUT_DIR, --output-dir OUTPUT_DIR
  -t TAG, --tag TAG
```

### configuration files

`detchar-hwinj-schedule` requires an INI file declaring the types of waveforms to be used and their parameters.
The syntax is fairly simple with a single section for each type of injection (e.g., combination of waveform parameters except for the central time) obeying the following pattern:

  * the section name is an arbitrary nickname that will be used to reference this waveform from `detchar-hwinj-schedule`'s command line; typically it should be clear what the waveform is from the nickname, but this is not required.
  * the `type` option is required and should be the name of a class declared in `detcharHWinj.waveforms` (technically, a subclass of `detcharHWinj.Waveform`). This tells the code which functional form will be used to generate the requested injection waveform.
  * a series of waveform-specific parameters, such as `a`, `f`, `tau`, and `phi` for `SineGaussianTau`. The values provided here will be unique to each section, and the set of required options may be different for different waveform families. The required options are declared for each waveform within the class's `required` attribute.

An example section is included below.
There is also an example INI available in `~/etc/example.ini`
```
[arbitrary waveform nickname]
type = SineGaussianTau

a = 1e-22
f = 100.
tau = 0.100
phi = 0.
```

### libraries

**utils**

This module houses general utilities useful throughout the library.
In particular, it declares the `Injection` and `InjectionSequence` classes, which make use of `detcharHWinj.waveforms.Waveform` objects to standardize how injected strains are computed and distributed; `detchar-hwinj-schedule` uses these a lot.
The module also contains some basic parsing routines for parameters extracted from the INI file and a convenience function that combines the injections from multiple `InjectionSequence` objects into a single list with appropriate times, etc.

**gracedb**

This module contains utilities for interacting with GraceDb.
This includes a wrapper for instantiating client objects (useful if we decide to add support for FakeDb) and a few interaction functions.
These functions standardize what is written to GraceDb for each HW injection, including things like JSON parameter filenames and file formats.

### dependencies

  * [detchar-waveforms](https://git.ligo.org/detchar/detchar-waveforms): used to generate time-domain representations of waveforms.
  * [ligo.gracedb](https://git.ligo.org/lscsoft/gracedb-client): used to automate annotations and queries to GraceDb.
  * [glue.ligolw](http://software.ligo.org/docs/glue/glue.ligolw-module.html): used to generate xml tables needed for event creation within 

### References

This section is mostly for those who will run the code.

  * When checking for whether injections were successful, we should use `?1:DMT-INJECTION_DETCHAR:1`.
  * When updating the schedule file within the SVN (`~/hwinj/Details/inj_trans/schedule.txt`), we need to use the following format
    * $START_GPS $INSTRUMENT INJECT_DETCHAR_ACTIVE $REQUIRE_OBSERVING_INTENT $I_HAVE_NO_IDEA relative/path/to/file.txt
    *  where REQUIRE_OBSERVING_INTENT should be 1.0 if we want to inject only into observing time.
